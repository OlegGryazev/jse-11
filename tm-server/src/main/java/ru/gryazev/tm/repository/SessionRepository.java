package ru.gryazev.tm.repository;

import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.entity.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

}

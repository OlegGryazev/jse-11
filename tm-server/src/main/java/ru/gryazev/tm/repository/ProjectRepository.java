package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.entity.Project;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findByName(@NotNull final String userId, @NotNull final String projectName) {
        return entities.values().stream().filter(o ->
                userId.equals(o.getUserId()) && o.getName() != null && o.getName().contains(projectName))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Project> findByDetails(@NotNull final String userId, @NotNull final String projectDetails) {
        return entities.values().stream().filter(o ->
                userId.equals(o.getUserId()) && o.getDetails() != null && o.getDetails().contains(projectDetails))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Project findProjectByIndex(@NotNull final String userId, final int index) {
        return entities.values().stream().filter(o -> userId.equals(o.getUserId()))
                .skip(index).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findProjectByIndex(@NotNull final String userId,
                                      final int index,
                                      @NotNull final Comparator<ComparableEntity> comparator) {
        return entities.values().stream().sorted(comparator).filter(o -> userId.equals(o.getUserId()))
                .skip(index).findFirst().orElse(null);
    }

}

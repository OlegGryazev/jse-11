package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.entity.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    public List<Project> findByName(@NotNull String userId, @NotNull String projectName);

    @NotNull
    public List<Project> findByDetails(@NotNull String userId, @NotNull String projectDetails);

    @Nullable
    public Project findProjectByIndex(@NotNull String userId, int index);

    @Nullable
    public Project findProjectByIndex(@NotNull String userId,
                                      int index,
                                      @NotNull Comparator<ComparableEntity> comparator);

}

package ru.gryazev.tm.api.repository;

import ru.gryazev.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {
}

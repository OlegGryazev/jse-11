package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull
    public List<Task> listTaskUnlinked(@Nullable String userId);

    public void removeByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    public Task unlinkTask(@Nullable String userId, @Nullable Task task);

    @Nullable
    public String getTaskId(@Nullable String userId,
                            @Nullable String projectId,
                            int taskIndex);

    @Nullable
    public String getTaskId(@Nullable String projectId,
                            @Nullable String userId,
                            int taskIndex,
                            @NotNull Comparator<ComparableEntity> comparator);

    @NotNull
    public List<Task> listTaskByProject(@Nullable String userId, @Nullable String projectId);

    @Nullable
    public Task linkTask(@Nullable String userId,
                         @Nullable String oldProjectId,
                         @Nullable String newProjectId,
                         int taskIndex);

    @NotNull
    public List<Task> findByName(@Nullable String userId, @Nullable String taskName);

    @NotNull
    public List<Task> findByDetails(@Nullable String userId, @Nullable String taskDetails);

    @NotNull
    public List<Task> listTaskByProjectSorted(@Nullable String userId,
                                              @Nullable String projectId,
                                              @Nullable String sortType);

    @NotNull
    List<Task> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType);

}

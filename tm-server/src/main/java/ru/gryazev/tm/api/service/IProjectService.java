package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.entity.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    public String getProjectId(@Nullable String userId, int projectIndex);

    @Nullable
    public String getProjectId(int projectIndex,
                               @Nullable String userId,
                               @NotNull Comparator<ComparableEntity> comparator);

    @NotNull
    public List<Project> findByName(@Nullable String userId, @Nullable String projectName);

    @NotNull
    public List<Project> findByDetails(@Nullable String userId, @Nullable String projectDetails);

    @NotNull
    public List<Project> findByUserIdSorted(@Nullable String userId, @Nullable String sortType);
}

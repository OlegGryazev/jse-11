package ru.gryazev.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.entity.Session;

public interface ServiceLocator {

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public ISessionService getSessionService();

    @NotNull
    public IDomainService getDomainService();

}

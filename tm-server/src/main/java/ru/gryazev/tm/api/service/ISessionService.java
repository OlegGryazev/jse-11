package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.enumerated.RoleType;

import java.io.IOException;

public interface ISessionService extends IService<Session> {

    @Nullable
    public Session create(@Nullable String login, @Nullable String pwdHash);

    public void setRepository(ISessionRepository repository);

    public void setUserRepository(IUserRepository userRepository);

    @Contract("null -> fail")
    public void validateSession(@Nullable Session session) throws Exception;

    @Contract("null -> fail")
    public void validateSession(@Nullable Session session, @Nullable RoleType[] roles) throws Exception;

}

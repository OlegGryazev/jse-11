package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;
    
    @NotNull
    @Override
    public List<Project> findAllProject(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        return serviceLocator.getProjectService().findAll();
    }

    @Nullable
    @Override
    public Project createProject(@Nullable final Session session, @Nullable final Project project) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().create(session.getUserId(), project);
    }

    @Nullable
    @Override
    public String getProjectId(@Nullable final Session session, final int projectIndex) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().getProjectId(session.getUserId(), projectIndex);
    }

    @Nullable
    @Override
    public Project findOneProject(@Nullable final Session session, @Nullable final String entityId) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().findOne(session.getUserId(), entityId);
    }

    @NotNull
    @Override
    public List<Project> findProjectByUserId(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().findByUserId(session.getUserId());
    }

    @Override
    public @NotNull List<Project> findProjectByUserIdSorted(@Nullable Session session, @Nullable String sortType) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().findByUserIdSorted(session.getUserId(), sortType);
    }

    @Nullable
    @Override
    public Project editProject(@Nullable final Session session, @Nullable final Project project) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().edit(session.getUserId(), project);
    }

    @NotNull
    @Override
    public List<Project> findProjectByDetails(@NotNull Session session, @NotNull String projectDetails) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().findByDetails(session.getUserId(), projectDetails);
    }

    @NotNull
    @Override
    public List<Project> findProjectByName(@Nullable Session session, @Nullable String projectName) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), projectName);
    }

    @Nullable
    @Override
    public Project removeProject(@Nullable final Session session, @Nullable final String entityId) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().remove(session.getUserId(), entityId);
    }

    @Override
    public void removeAllProject(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }
    
    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

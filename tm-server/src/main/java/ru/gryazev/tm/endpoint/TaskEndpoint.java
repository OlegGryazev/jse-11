package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    @Nullable
    @Override
    public Task findOneTask(@Nullable final Session session, @Nullable final String taskId) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findOne(session.getUserId(), taskId);
    }

    @Nullable
    @Override
    public Task createTask(@Nullable final Session session, @Nullable final Task task) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().create(session.getUserId(), task);
    }

    @Override
    public void removeAllTask(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().removeAll(session.getUserId());
    }

    @Nullable
    @Override
    public Task editTask(@Nullable final Session session, @Nullable final Task task) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().edit(session.getUserId(), task);
    }

    @Nullable
    @Override
    public Task removeTask(@Nullable final Session session, @Nullable final String taskId) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().remove(session.getUserId(), taskId);
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final Session session, @Nullable final String projectId, final int taskIndex) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().getTaskId(session.getUserId(), projectId, taskIndex);
    }

    @NotNull
    @Override
    public List<Task> findTaskByName(@Nullable final Session session, @Nullable final String taskName) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), taskName);
    }

    @NotNull
    @Override
    public List<Task> findTaskByDetails(@Nullable final Session session, @Nullable final String taskDetails) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findByDetails(session.getUserId(), taskDetails);
    }

    @Nullable
    @Override
    public Task linkTask(@Nullable final Session session,
                         @Nullable final String oldProjectId,
                         @Nullable final String newProjectId,
                         final int taskIndex) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().linkTask(session.getUserId(), oldProjectId, newProjectId, taskIndex);
    }

    @Override
    public @NotNull List<Task> findTaskByProjectSorted(@Nullable Session session, @Nullable String projectId, @Nullable String sortType) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskByProjectSorted(session.getUserId(), projectId, sortType);
    }

    @Override
    public @NotNull List<Task> listTaskUnlinkedSorted(@Nullable Session session, @Nullable String sortType) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskUnlinkedSorted(session.getUserId(), sortType);
    }

    @NotNull
    @Override
    public List<Task> listTaskByProject(@Nullable final Session session, @Nullable final String projectId) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskByProject(session.getUserId(), projectId);
    }

    @NotNull
    @Override
    public  List<Task> listTaskUnlinked(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskUnlinked(session.getUserId());
    }

    @Override
    public void removeByProjectId(@Nullable final Session session, @Nullable final String projectId) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().removeByProjectId(session.getUserId(), projectId);
    }

    @Nullable
    @Override
    public Task unlinkTask(@Nullable final Session session, @Nullable final Task task) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().unlinkTask(session.getUserId(), task);
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

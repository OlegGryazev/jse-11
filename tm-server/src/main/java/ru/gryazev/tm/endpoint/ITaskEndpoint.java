package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.Task;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    public Task findOneTask(@Nullable Session session, @Nullable String taskId) throws Exception;

    @Nullable
    public Task createTask(@Nullable Session session, @Nullable Task task) throws Exception;

    public void removeAllTask(@Nullable Session session) throws Exception;

    @Nullable
    public Task editTask(@Nullable Session session, @Nullable Task task) throws Exception;

    @Nullable
    public Task removeTask(@Nullable Session session, @Nullable String taskId) throws Exception;

    @Nullable
    public String getTaskId(@Nullable Session session,
                            @Nullable String projectId,
                            int taskIndex) throws Exception;

    @Nullable
    public Task unlinkTask(@Nullable Session session, @Nullable Task task) throws Exception;

    @NotNull
    public List<Task> findTaskByName(@Nullable Session session, @Nullable String taskName) throws Exception;

    @NotNull
    public List<Task> findTaskByDetails(@Nullable Session session, @Nullable String taskDetails) throws Exception;

    @Nullable
    public Task linkTask(@Nullable Session session,
                         @Nullable String oldProjectId,
                         @Nullable String newProjectId,
                         int taskIndex) throws Exception;

    @NotNull
    public List<Task> listTaskByProject(@Nullable Session session, @Nullable String projectId) throws Exception;

    @NotNull
    public List<Task> findTaskByProjectSorted(@Nullable Session session,
                                              @Nullable String projectId,
                                              @Nullable String sortType) throws Exception;

    @NotNull
    public List<Task> listTaskUnlinked(@Nullable Session session) throws Exception;

    public void removeByProjectId(@Nullable Session session, @Nullable String projectId) throws Exception;

    @NotNull
    public List<Task> listTaskUnlinkedSorted(@Nullable Session session, @Nullable String sortType) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}

package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public User findOneUser(@Nullable final Session session, @Nullable final String userId) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().findOne(session.getUserId(), userId);
    }

    @Nullable
    @Override
    public User createUser(@Nullable User user) {
        if (serviceLocator == null || user == null) return null;
        return serviceLocator.getUserService().create(user.getId(), user);
    }

    @Nullable
    @Override
    public User editUser(@Nullable final Session session, @Nullable final User user) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().edit(session.getUserId(), user);
    }

    @NotNull
    @Override
    public List<User> findAllUser(@Nullable Session session) throws Exception {
        if (serviceLocator == null) return Collections.emptyList();
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        return serviceLocator.getUserService().findAll();
    }

    @Nullable
    @Override
    public User removeUser(@Nullable Session session, @Nullable String userId) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().remove(session.getUserId(), userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable Session session, int userIndex) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().getUserId(userIndex);
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

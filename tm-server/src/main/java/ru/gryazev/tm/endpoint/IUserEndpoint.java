package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.User;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable
    public User findOneUser(@Nullable Session session, @Nullable String userId) throws Exception;

    @Nullable
    public User editUser(@Nullable Session session, @Nullable User user) throws Exception;

    @Nullable
    public User createUser(@Nullable User user);

    @Nullable
    public User removeUser(@Nullable Session session, @Nullable String userId) throws Exception;

    @Nullable
    public String getUserId(@Nullable Session session, int userIndex) throws Exception;

    @NotNull
    public List<User> findAllUser(@Nullable Session session) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}

package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    @Override
    public void saveJsonJaxb(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().saveJsonJaxb();
    }

    @Override
    public void saveXmlJaxb(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().saveXmlJaxb();
    }

    @Override
    public void saveJsonFasterxml(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().saveJsonFasterxml();
    }

    @Override
    public void saveXmlFasterxml(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().saveXmlFasterxml();
    }

    @Override
    public void saveSer(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().saveSer();
    }

    @Override
    public void loadJsonJaxb(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().loadJsonJaxb();
    }

    @Override
    public void loadXmlJaxb(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().loadXmlJaxb();
    }

    @Override
    public void loadJsonFasterxml(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().loadJsonFasterxml();
    }

    @Override
    public void loadXmlFasterxml(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().loadXmlFasterxml();
    }

    @Override
    public void loadSer(@Nullable final Session session) throws Exception {
        if (serviceLocator == null) return;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getDomainService().loadSer();
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

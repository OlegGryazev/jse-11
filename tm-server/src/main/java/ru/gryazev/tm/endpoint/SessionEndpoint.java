package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    @Nullable
    @Override
    public Session findOneSession(@Nullable final Session session, @NotNull final String userId, @NotNull final String id) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        return serviceLocator.getSessionService().findOne(userId, id);
    }

    @Nullable
    @Override
    public Session removeSession(@NotNull final Session session) throws Exception {
        if (serviceLocator == null) return null;
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getSessionService().remove(session.getUserId(), session.getId());
    }

    @Nullable
    @Override
    public Session createSession(@Nullable final String login, @Nullable final String pwdHash) {
        if (serviceLocator == null) return null;
        return serviceLocator.getSessionService().create(login, pwdHash);
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

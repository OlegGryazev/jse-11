package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Session;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    public List<Project> findAllProject(@Nullable Session session) throws Exception;

    @Nullable
    public Project createProject(@Nullable Session session, @Nullable Project project) throws Exception;

    @Nullable
    public Project findOneProject(@Nullable Session session, @Nullable String entityId) throws Exception;

    @NotNull
    public List<Project> findProjectByUserId(@Nullable Session session) throws Exception;

    @NotNull
    public List<Project> findProjectByUserIdSorted(@Nullable Session session, @Nullable String sortType) throws Exception;

    @Nullable
    public Project editProject(@Nullable Session session, @Nullable Project project) throws Exception;

    @Nullable
    public String getProjectId(@Nullable Session session, int projectIndex) throws Exception;

    @Nullable
    public Project removeProject(@Nullable Session session, @Nullable String entityId) throws Exception;

    public void removeAllProject(@Nullable Session session) throws Exception;

    @NotNull
    public List<Project> findProjectByDetails(@NotNull Session session, @NotNull String projectDetails) throws Exception;

    @NotNull
    public List<Project> findProjectByName(@Nullable Session session, @Nullable String projectName) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}

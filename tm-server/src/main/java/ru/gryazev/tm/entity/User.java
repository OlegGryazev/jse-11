package ru.gryazev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.enumerated.RoleType;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractCrudEntity {

    @Nullable
    private String login = "";

    @Nullable
    private String pwdHash = "";

    @Nullable
    private RoleType roleType;

    @NotNull
    @Override
    @JsonIgnore
    public String getUserId() {
        return getId();
    }

}

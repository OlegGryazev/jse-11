package ru.gryazev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.enumerated.RoleType;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractCrudEntity {

    @Nullable
    String userId;

    @Nullable
    RoleType role;

    @Nullable
    @JsonIgnore
    String signature;

    @JsonIgnore
    final long timestamp = new Date().getTime();

}

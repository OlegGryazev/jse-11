package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractCrudEntity implements ComparableEntity {

    @Nullable
    private String userId = null;

    @Nullable
    private String projectId = null;

    @Nullable
    private String name = "";

    @Nullable
    private String details = "";

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    private final Long createMillis = new Date().getTime();

}

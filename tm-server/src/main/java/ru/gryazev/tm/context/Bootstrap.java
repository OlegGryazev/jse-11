package ru.gryazev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.SessionRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService();

    @NotNull
    private ISessionEndpoint sessionEndpoint = new SessionEndpoint();

    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpoint();

    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpoint();

    @NotNull
    private IDomainEndpoint domainEndpoint = new DomainEndpoint();

    public void init() {
        sessionService.setUserRepository(userRepository);
        domainService.setServiceLocator(this);
        usersInit();
        projectEndpoint.setServiceLocator(this);
        taskEndpoint.setServiceLocator(this);
        userEndpoint.setServiceLocator(this);
        sessionEndpoint.setServiceLocator(this);
        domainEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/session?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/project?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/task?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/user?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/domain?wsdl", domainEndpoint);
    }

    private void usersInit() {
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
        user.setRoleType(RoleType.USER);
        userService.create(user.getId(), user);

        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
        admin.setRoleType(RoleType.ADMIN);
        userService.create(admin.getId(), admin);
    }

}
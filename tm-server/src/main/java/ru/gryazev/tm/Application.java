package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.context.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}

package ru.gryazev.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.omg.SendingContext.RunTimeOperations;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

@Setter
@NoArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Getter
    @Nullable
    private ISessionRepository repository;

    @Nullable
    private IUserRepository userRepository;

    public SessionService(@Nullable final ISessionRepository repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public Session create(@Nullable final String login, @Nullable final String pwdHash) {
        if (repository == null || userRepository == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (pwdHash == null || pwdHash.isEmpty()) return null;
        @Nullable final User user = userRepository.findByLoginAndPwd(login, pwdHash);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setRole(user.getRoleType());
        session.setUserId(user.getId());
        session.setSignature(SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT));
        return repository.persist(user.getId(), session);
    }

    @Override
    public boolean isEntityValid(@Nullable final Session session) {
        if (session == null || session.getUserId() == null || session.getUserId().isEmpty()) return false;
        if (session.getTimestamp() == 0) return false;
        return (session.getSignature() != null && !session.getSignature().isEmpty());
    }

    public void validateSession(@Nullable final Session session, @Nullable RoleType[] roles) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null) throw new Exception(message);
        if (roles == null || !Arrays.asList(roles).contains(session.getRole())) throw new Exception(message);
        validateSession(session);
    }

    public void validateSession(@Nullable final Session session) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null || session.getUserId() == null || session.getUserId().isEmpty()) throw new Exception(message);
        @NotNull final long sessionAliveTime = new Date().getTime() - session.getTimestamp();
        if (sessionAliveTime > Constant.SESSION_LIFETIME) throw new Exception(message);
        if (session.getRole() == null) throw new Exception(message);
        @Nullable final Session sessionAtServer = findOne(session.getUserId(), session.getId());
        if (sessionAtServer == null || sessionAtServer.getSignature() == null) throw new Exception(message);
        @Nullable final String actualSessionSignature =  SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT);
        if (!sessionAtServer.getSignature().equals(actualSessionSignature)) throw new Exception(message);
    }

}

package ru.gryazev.tm.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.api.service.IService;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractService <T extends AbstractCrudEntity> implements IService<T> {

    @Nullable
    @Override
    public T create(@Nullable final String userId, @Nullable final T t) {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(t)) return null;
        return getRepository().persist(userId, t);
    }

    @Nullable
    @Override
    public T findOne(@Nullable final String userId, @Nullable final String entityId) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityId == null || entityId.isEmpty()) return null;
        return getRepository().findOne(userId, entityId);
    }

    @NotNull
    @Override
    public List<T> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return new ArrayList<T>(getRepository().findAll(userId).values());
    }

    @Nullable
    @Override
    public T edit(@Nullable final String userId, @Nullable final T t) {
        if (!isEntityValid(t)) return null;
        if (userId == null || userId.isEmpty()) return null;
        return getRepository().merge(userId, t);
    }

    @Nullable
    @Override
    public T remove(@Nullable final String userId, @Nullable final String entityId) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityId == null || entityId.isEmpty()) return null;
        return getRepository().remove(userId, entityId);
    }

    @Override
    public void removeAll(@Nullable final String userId){
        if (userId == null || userId.isEmpty()) return;
        getRepository().removeAll(userId);
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }

    @NotNull
    @Override
    public List<T> findAll() {
        return getRepository().findAll();
    }


    @Contract("null -> false")
    public abstract boolean isEntityValid(@Nullable final T t);

    @NotNull
    public abstract IRepository<T> getRepository();

}

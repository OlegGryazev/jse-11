package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.comparator.ComparableEntityComparator;
import ru.gryazev.tm.entity.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @Getter
    @NotNull
    final ITaskRepository repository;

    @NotNull
    @Override
    public List<Task> listTaskUnlinked(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return repository.findUnlinked(userId);
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        for (@NotNull final Task task : repository.findTasksByProjectId(userId, projectId))
            repository.remove(userId, task.getId());
    }

    @Nullable
    @Override
    public Task unlinkTask(@Nullable final String userId, @Nullable final Task task) {
        if (!isEntityValid(task)) return null;
        if (userId == null || userId.isEmpty()) return null;
        task.setProjectId(null);
        return repository.merge(userId, task);
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String userId, @Nullable final String projectId, final int taskIndex) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @Nullable final Task task = repository.findTaskByIndex(projectId, userId, taskIndex);
        if (task == null) return null;
        return task.getId();
    }

    @NotNull
    @Override
    public List<Task> listTaskByProject(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findTasksByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task linkTask(@Nullable final String userId,
                         @Nullable final String oldProjectId,
                         @Nullable final String newProjectId,
                         final int taskIndex) {
        if (userId == null || userId.isEmpty()) return null;
        if (newProjectId == null || newProjectId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @Nullable final Task task = oldProjectId == null ?
                repository.findUnlinkedTaskByIndex(userId, taskIndex) :
                repository.findTaskByIndex(oldProjectId, userId, taskIndex);
        if (task == null) return null;
        task.setProjectId(newProjectId);
        return repository.merge(userId, task);
    }

    @NotNull
    @Override
    public List<Task> findByName(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        return repository.findByName(userId, taskName);
    }

    @NotNull
    @Override
    public List<Task> findByDetails(@Nullable final String userId, @Nullable final String taskDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskDetails == null || taskDetails.isEmpty()) return Collections.emptyList();
        return repository.findByDetails(userId, taskDetails);
    }

    @NotNull
    @Override
    public List<Task> listTaskByProjectSorted(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String sortType
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final Comparator<ComparableEntity> comparator = ComparableEntityComparator.getComparator(sortType);
        @NotNull final List<Task> tasks = repository.findTasksByProjectId(userId, projectId);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public @NotNull List<Task> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Comparator<ComparableEntity> comparator = ComparableEntityComparator.getComparator(sortType);
        @NotNull final List<Task> tasks = repository.findUnlinked(userId);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public boolean isEntityValid(@Nullable final Task task) {
        if (task == null) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (task.getUserId() == null || task.getUserId().isEmpty()) return false;
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) return false;
        return (task.getName() != null && !task.getName().isEmpty());
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String projectId,
                            @Nullable final String userId,
                            final int taskIndex,
                            @NotNull final Comparator<ComparableEntity> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @Nullable final Task task = repository.findTaskByIndex(projectId, userId, taskIndex, comparator);
        if (task == null) return null;
        return task.getId();
    }

}
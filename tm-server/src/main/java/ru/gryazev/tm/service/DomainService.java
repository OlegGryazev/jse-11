package ru.gryazev.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Setter;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class DomainService implements IDomainService {

    @Setter
    @Nullable
    private ServiceLocator serviceLocator;

    @Override
    public void saveJsonJaxb() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final Domain domain = prepareDomain();
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final String fileName = Constant.DATA_DIR + "data-json.json";
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdir();
        marshaller.marshal(domain, file);
    }

    @Override
    public void saveXmlJaxb() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final Domain domain = prepareDomain();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final String fileName = Constant.DATA_DIR + "data-xml.xml";
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdir();
        marshaller.marshal(domain, file);
    }

    @Override
    public void saveJsonFasterxml() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
        @NotNull final Domain domain = prepareDomain();
        @NotNull final String fileName = Constant.DATA_DIR + "data-json.json";
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdir();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    }

    @Override
    public void saveXmlFasterxml() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = prepareDomain();
        @NotNull final String fileName = Constant.DATA_DIR + "data-xml.xml";
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdir();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    }

    @Override
    public void saveSer() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final Domain domain = prepareDomain();
        @NotNull final String fileName = Constant.DATA_DIR + "data.ser";
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdir();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    @Override
    public void loadJsonJaxb() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final String fileName = Constant.DATA_DIR + "data-json.json";
        @NotNull final File file = new File(fileName);
        @NotNull final StreamSource reader = new StreamSource(file);
        @NotNull final Domain domain = unmarshaller.unmarshal(reader, Domain.class).getValue();
        loadDataFromDomain(domain);
    }

    @Override
    public void loadXmlJaxb() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final String fileName = Constant.DATA_DIR + "data-xml.xml";
        @NotNull final File file = new File(fileName);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        loadDataFromDomain(domain);
    }

    @Override
    public void loadJsonFasterxml() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final String fileName = Constant.DATA_DIR + "data-json.json";
        @NotNull final File file = new File(fileName);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        @NotNull final Domain domain = mapper.readValue(file, Domain.class);
        loadDataFromDomain(domain);
    }

    @Override
    public void loadXmlFasterxml() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final String fileName = Constant.DATA_DIR + "data-xml.xml";
        @NotNull final File file = new File(fileName);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(file, Domain.class);
        loadDataFromDomain(domain);
    }

    @Override
    public void loadSer() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final String fileName = Constant.DATA_DIR + "data.ser";
        @NotNull final FileInputStream fileInputStream = new FileInputStream(fileName);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        loadDataFromDomain(domain);
        objectInputStream.close();
    }

    private void loadDataFromDomain(@NotNull final Domain domain) throws Exception {
        if (serviceLocator == null) throw new Exception("Error during load operation.");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final ISessionService sessionService = serviceLocator.getSessionService();
        projectService.removeAll();
        taskService.removeAll();
        userService.removeAll();
        sessionService.removeAll();
        domain.getProjects().forEach(o -> projectService.create(o.getUserId(), o));
        domain.getTasks().forEach(o -> taskService.create(o.getUserId(), o));
        domain.getUsers().forEach(o -> userService.create(o.getUserId(), o));
    }

    @NotNull
    private Domain prepareDomain() throws Exception {
        if (serviceLocator == null) throw new Exception("Error during save operation.");
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

}

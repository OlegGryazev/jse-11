package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.comparator.ComparableEntityComparator;
import ru.gryazev.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Getter
    @NotNull
    private final IProjectRepository repository;

    @Nullable
    public String getProjectId(@Nullable final String userId, final int projectIndex){
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        Project project = repository.findProjectByIndex(userId, projectIndex);
        if (project == null) return null;
        return project.getId();
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        return repository.findByName(userId, projectName);
    }

    @NotNull
    @Override
    public List<Project> findByDetails(@Nullable final String userId, @Nullable final String projectDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectDetails == null || projectDetails.isEmpty()) return Collections.emptyList();
        return repository.findByDetails(userId, projectDetails);
    }

    @NotNull
    @Override
    public List<Project> findByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull Comparator<ComparableEntity> comparator = ComparableEntityComparator.getComparator(sortType);
        @NotNull final ArrayList<Project> projects = new ArrayList<>(getRepository().findAll(userId).values());
        projects.sort(comparator);
        return projects;
    }

    @Override
    public boolean isEntityValid(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        if (project.getName() == null || project.getName().isEmpty()) return false;
        return project.getUserId() != null && !project.getUserId().isEmpty();
    }

    @Nullable
    @Override
    public String getProjectId(final int projectIndex, @Nullable final String userId, @NotNull final Comparator<ComparableEntity> comparator) {
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        Project project = repository.findProjectByIndex(userId, projectIndex, comparator);
        if (project == null) return null;
        return project.getId();
    }

}

package ru.gryazev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;

import java.util.Comparator;

public class ComparableEntityComparator {

    @NotNull
    public static final Comparator<ComparableEntity> comparatorCreateMillis = Comparator.comparingLong(ComparableEntity::getCreateMillis);

    @NotNull
    public static final Comparator<ComparableEntity> comparatorDateStart = new Comparator<ComparableEntity>() {
        @Override
        public int compare(@NotNull ComparableEntity o1, @NotNull ComparableEntity o2) {
            if (o1.getDateStart() == null || o2.getDateStart() == null) return 0;
            return o1.getDateStart().compareTo(o2.getDateStart());
        }
    };

    @NotNull
    public static final Comparator<ComparableEntity> comparatorDateFinish = new Comparator<ComparableEntity>() {
        @Override
        public int compare(@NotNull ComparableEntity o1, @NotNull ComparableEntity o2) {
            if (o1.getDateFinish() == null || o2.getDateFinish() == null) return 0;
            return o1.getDateFinish().compareTo(o2.getDateFinish());
        }
    };

    @NotNull
    public static final Comparator<ComparableEntity> comparatorStatus = Comparator.comparing(ComparableEntity::getStatus);

    @NotNull
    public static Comparator<ComparableEntity> getComparator(@Nullable final String name) {
        if (name == null) return comparatorCreateMillis;
        switch (name) {
            case "default": return comparatorCreateMillis;
            case "date-start": return comparatorDateStart;
            case "date-finish": return comparatorDateFinish;
            case "status": return comparatorStatus;
        }
        return comparatorCreateMillis;
    }

}

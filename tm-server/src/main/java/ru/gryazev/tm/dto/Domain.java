package ru.gryazev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@JsonIgnoreProperties
@JsonRootName(value = "domain")
@XmlRootElement(name = "domain")
@JacksonXmlRootElement(localName = "domain")
public final class Domain implements Serializable {

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<User> users = new ArrayList<>();

    public void setProjects(@Nullable List<Project> projects) {
        if (projects == null) return;
        this.projects = projects;
    }

    public void setTasks(@Nullable List<Task> tasks) {
        if (tasks == null) return;
        this.tasks = tasks;
    }

    public void setUsers(@Nullable List<User> users) {
        if (users == null) return;
        this.users = users;
    }

    @NotNull
    @JacksonXmlElementWrapper(useWrapping = false)
    public List<Project> getProjects() {
        return projects;
    }

    @NotNull
    @JacksonXmlElementWrapper(useWrapping = false)
    public List<Task> getTasks() {
        return tasks;
    }

    @NotNull
    @JacksonXmlElementWrapper(useWrapping = false)
    public List<User> getUsers() {
        return users;
    }

}

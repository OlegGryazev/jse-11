package ru.gryazev.tm.error;

public class CrudException extends RuntimeException {

    CrudException(String message) {
        super(message);
    }

}

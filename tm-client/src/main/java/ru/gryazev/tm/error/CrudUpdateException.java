package ru.gryazev.tm.error;

public final class CrudUpdateException extends CrudException{

    public CrudUpdateException() {
        super("Error during update operation");
    }

}

package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;
import ru.gryazev.tm.endpoint.Session;

public class SerSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save all data using serialization.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final Session session = getSession();
        serviceLocator.getDomainEndpoint().saveSer(session);
        terminalService.print("[OK]");
    }

}

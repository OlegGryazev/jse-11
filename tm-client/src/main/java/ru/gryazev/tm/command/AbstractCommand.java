package ru.gryazev.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.CommandLocator;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.context.SessionLocator;
import ru.gryazev.tm.api.service.ISettingService;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.error.CrudInitializationException;
import ru.gryazev.tm.error.CrudUpdateException;

@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    protected ITerminalService terminalService;

    @Nullable
    protected ISettingService settingService;

    @Nullable
    protected CommandLocator commandLocator;

    @Nullable
    protected SessionLocator sessionLocator;

    @Getter
    private boolean allowed = false;

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    @NotNull
    public Session getSession() {
        if (sessionLocator == null) throw new CrudInitializationException();
        @Nullable final Session session = sessionLocator.getSession();
        if (session == null) throw new CrudUpdateException();
        return session;
    }

}

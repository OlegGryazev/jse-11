package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;
import ru.gryazev.tm.endpoint.Session;

public class JsonLoadJaxbCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "load-json-jaxb";
    }

    @Override
    public String getDescription() {
        return "Loads data from JSON using JAXB.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final Session session = getSession();
        serviceLocator.getDomainEndpoint().loadJsonJaxb(session);
        terminalService.print("[OK]");
    }

}

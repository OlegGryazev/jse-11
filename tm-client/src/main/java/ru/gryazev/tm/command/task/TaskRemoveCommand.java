package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudDeleteException;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task from selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final Session session = getSession();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String projectId = getProjectId();
        @Nullable final String taskId = taskEndpoint.getTaskId(session, projectId, taskIndex);

        @Nullable final Task removedTask = taskEndpoint.removeTask(session, taskId);
        if (removedTask == null) throw new CrudDeleteException();
        terminalService.print("[DELETED]");
    }

}

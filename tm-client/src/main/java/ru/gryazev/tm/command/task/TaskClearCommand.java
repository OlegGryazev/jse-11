package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.endpoint.Session;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Clear tasks list.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final Session session = getSession();
        serviceLocator.getTaskEndpoint().removeAllTask(session);
        terminalService.print("[CLEAR OK]");
    }

}

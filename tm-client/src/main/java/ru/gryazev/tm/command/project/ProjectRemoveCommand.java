package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.error.CrudDeleteException;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Session session = getSession();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectEndpoint.getProjectId(session, projectIndex);

        @Nullable final Project removedProject = projectEndpoint.removeProject(session, projectId);
        if (removedProject == null) throw new CrudDeleteException();
        serviceLocator.getTaskEndpoint().removeByProjectId(session, projectId);
        terminalService.print("[DELETED]");
    }

}

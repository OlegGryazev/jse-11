package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.User;

@NoArgsConstructor
public class UserListCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Shows all users (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        for (@NotNull final User user : serviceLocator.getUserEndpoint().findAllUser(getSession()))
            terminalService.print(user.getLogin());
    }

}

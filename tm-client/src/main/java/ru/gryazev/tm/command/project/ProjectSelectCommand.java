package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.entity.Setting;

@NoArgsConstructor
public final class ProjectSelectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Select project. To reset, type \"-1\".";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final Session session = getSession();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = serviceLocator.getProjectEndpoint()
                .getProjectId(session, projectIndex);
        serviceLocator.getSettingService().edit(session.getUserId(), new Setting("current-project", projectId));
    }

}

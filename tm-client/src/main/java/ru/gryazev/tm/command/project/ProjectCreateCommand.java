package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.error.CrudCreateException;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final Session session = getSession();
        terminalService.print("[PROJECT CREATE]");
        @NotNull final Project project = terminalService.getProjectFromConsole();
        project.setUserId(session.getUserId());
        @Nullable final Project createdProject = serviceLocator.getProjectEndpoint().createProject(session, project);
        if (createdProject == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}

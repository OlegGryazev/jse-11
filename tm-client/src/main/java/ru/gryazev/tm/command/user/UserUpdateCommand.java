package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class UserUpdateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final Session session = getSession();
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @Nullable final String pwd = terminalService.getPwdHashFromConsole();
        @Nullable final String pwdRepeat = terminalService.getPwdHashFromConsole();
        if (!pwd.equals(pwdRepeat)){
            terminalService.print("Entered passwords do not match!");
            return;
        }

        @Nullable final User user = userEndpoint.findOneUser(session, session.getUserId());
        if (user == null) throw new CrudNotFoundException();
        user.setPwdHash(pwd);
        @Nullable final User editedUser = userEndpoint.editUser(session, user);
        if (editedUser == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}

package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Session;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear projects list.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final Session session = getSession();
        serviceLocator.getProjectEndpoint().removeAllProject(session);
        serviceLocator.getTaskEndpoint().removeAllTask(session);
        terminalService.print("[CLEAR OK]");
    }

}

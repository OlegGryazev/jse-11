package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.error.CrudNotFoundException;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null || serviceLocator == null || sessionLocator == null) return;
        @Nullable final Session session = getSession();
        @Nullable final Session removedSession = serviceLocator.getSessionEndpoint().removeSession(session);
        if (removedSession == null) throw new CrudNotFoundException();
        sessionLocator.setSession(null);
        terminalService.print("You are logged out.");
    }

}

package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Session session = getSession();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectEndpoint.getProjectId(session, projectIndex);
        if (projectId == null) throw new CrudNotFoundException();

        @NotNull final Project project = terminalService.getProjectFromConsole();
        project.setId(projectId);
        project.setUserId(session.getUserId());
        @Nullable final Project editedProject = projectEndpoint.editProject(session, project);
        if (editedProject == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}

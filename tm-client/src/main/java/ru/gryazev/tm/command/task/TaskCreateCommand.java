package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudCreateException;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task at selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final Session session = getSession();
        terminalService.print("[TASK CREATE]");
        @NotNull final Task task = terminalService.getTaskFromConsole();
        @Nullable String currentProjectId = getProjectId();
        task.setUserId(session.getUserId());
        task.setProjectId(currentProjectId);
        @Nullable final Task createdTask = serviceLocator.getTaskEndpoint().createTask(session, task);
        if (createdTask == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}

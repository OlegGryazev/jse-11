package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;
import ru.gryazev.tm.endpoint.Session;

public class XmlLoadFasterxmlCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "load-xml-fasterxml";
    }

    @Override
    public String getDescription() {
        return "Loads data from XML using Fasterxml.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final Session session = getSession();
        serviceLocator.getDomainEndpoint().loadXmlFasterxml(session);
        terminalService.print("[OK]");
    }

}

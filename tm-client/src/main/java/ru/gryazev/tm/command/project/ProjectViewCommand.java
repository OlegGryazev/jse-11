package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.error.CrudNotFoundException;

@NoArgsConstructor
public final class ProjectViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-view";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final IProjectEndpoint projectServiceEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final Session session = getSession();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectServiceEndpoint.getProjectId(session, projectIndex);

        @Nullable final Project project = projectServiceEndpoint.findOneProject(session, projectId);
        if (project == null) throw new CrudNotFoundException();
        terminalService.printProject(project);
    }

}

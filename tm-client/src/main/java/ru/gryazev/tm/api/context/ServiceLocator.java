package ru.gryazev.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.ISettingService;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.endpoint.*;

public interface ServiceLocator {

    @NotNull
    public ISessionEndpoint getSessionEndpoint();

    @NotNull
    public IProjectEndpoint getProjectEndpoint();

    @NotNull
    public ITaskEndpoint getTaskEndpoint();

    @NotNull
    public IUserEndpoint getUserEndpoint();

    @NotNull
    public IDomainEndpoint getDomainEndpoint();

    @NotNull
    public ITerminalService getTerminalService();

    @NotNull
    public ISettingService getSettingService();

}

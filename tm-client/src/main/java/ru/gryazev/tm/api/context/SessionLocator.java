package ru.gryazev.tm.api.context;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.Session;

public interface SessionLocator {

    public void setSession(@Nullable Session session);

    @Nullable
    public Session getSession();

}
